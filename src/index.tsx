/*
 * @Author: Monstar 10187267+monstartian@user.noreply.gitee.com
 * @Date: 2022-09-16 14:48:20
 * @LastEditors: Monstar 10187267+monstartian@user.noreply.gitee.com
 * @LastEditTime: 2022-09-17 09:54:24
 * @FilePath: \田瑞普\src\index.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createRoot } from "react-dom/client";
import useProtal from "./customHooks/useProtal";
import { animated, useSpring } from "react-spring"; // 动画库
import styles from "./style.module.css";
import React from "react";
import OptimizedReactiveHeight from "./customHooks/insert"
function App() {
  const { Protal, handleClose, handleShow, isShow } = useProtal();
  const animation = useSpring({
    reverse: !isShow,
    to: { opacity: 1 },
    from: { opacity: 0 },
  });
  const [val, setVal] = React.useState(0);
  const [num, setNum] = React.useState(0);
  const [num2, setNum2] = React.useState(0);

  return (
    <>
      {/* <div>状态：{JSON.stringify(isShow)}</div> */}
      <div>总数：{JSON.stringify(num)}</div>
      <button onClick={handleShow}>弹出弹框</button>
      <Protal>
        <animated.div style={animation} className={styles.mark}>
          <div className={styles.close} >
            <button onClick={handleClose}>关闭</button>
          </div>
          <div>
            <input type="number" className={styles.input} value={val} onChange={(e) => {
              try {
                setVal(parseInt(e.target.value));
                setNum2(parseInt(e.target.value) * 2)
              } catch (error: any) {
                console.log(error);
                throw new Error('');
              }
            }} />
          </div>
          <p>双倍显示：{num2 ? num2 : 0}</p>
          <div className={styles.buttom} >
            <button onClick={() => {
              // 清空
              console.log('清空');
              setVal(0);
              setNum2(0)
            }} >清空</button>
            <button onClick={() => {
              setNum(num + num2 / 2);
              console.log(num);

              handleClose();
            }} >确定</button>
          </div>
        </animated.div>
      </Protal>
      <OptimizedReactiveHeight />


    </>
  );
}
const container = document.getElementById("root") as HTMLElement;
const root = createRoot(container);
// const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);

