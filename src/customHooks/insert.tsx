import React, { useState, useEffect, useRef, useLayoutEffect, useCallback } from 'react';
import LoadMore from "./loadMore"
interface IProps<T> {
    data: T[];
    key?: string;
    maxPages: number;
    itemRender: (item: T, index: number, childIndex: number) => JSX.Element;
    reachBottom: () => any;
}
type loadProps = 'more' | 'loading' | 'noMore';

export default function OptimizedReactiveHeight<T>({
    data,
    key = 'id',
    itemRender,
    maxPages = 0,
    reachBottom = () => { },
}: IProps<T>) {
    const isCanCreat = useRef<boolean>(true);
    const newPageCreat = useRef<boolean>(false);
    const contentObserver = useRef<IntersectionObserver | null>(null);
    const footerObserver = useRef<IntersectionObserver | null>(null);
    const trueData = useRef<Array<Array<unknown>>>([data]);
    const virtualTrueRef = useRef<Array<Array<any>>>([data]);
    const statusRef = useRef<loadProps>('more');

    const [virtualTrueData, setVirtualTrueData] = useState<Array<Array<any>>>([data]);
    const [listStatus, setListStatus] = useState<loadProps>('more');

    useLayoutEffect(() => {
        if (isCanCreat.current) {
            // 创建分页监听
            isCanCreat.current = false;
            contentObserver.current = new IntersectionObserver(entries => {
                if (newPageCreat.current) {
                    newPageCreat.current = false;
                    return;
                }
                console.log('content entries', entries);
                entries.forEach((item: any) => {
                    if (item.intersectionRatio <= 0) {
                        virtualTrueRef.current[item.target.id] = [];
                        setVirtualTrueData([...virtualTrueRef.current]);
                    } else {
                        virtualTrueRef.current[item.target.id] = trueData.current[item.target.id];
                        setVirtualTrueData([...virtualTrueRef.current]);
                    }
                });
            }, {});
            // 脱离更新机制给节点添加高度
            setTimeout(() => {
                const listEl: any = document.getElementsByClassName('visible-list');
                for (let i = 0; i < listEl.length; i++) {
                    const height = listEl?.[i].offsetHeight || 200;
                    listEl[i].style.height = `${height}px`;
                    // @ts-ignore
                    contentObserver.current?.observe(listEl?.[i]);
                }
            }, 0);
            // 添加触底监听
            const footEl = document.getElementsByClassName('list-footer');
            footerObserver.current = new IntersectionObserver(entries => {
                console.log('footer entries', entries);
                // 加载下一页
                if (entries[0].intersectionRatio > 0) {
                    onReachBottom();
                }
            }, {});
            // @ts-ignore
            footerObserver.current.observe(footEl[0]);
        }

        return () => {
            // @ts-ignore
            contentObserver.current.disconnect();
        };
    }, []);

    const onReachBottom = useCallback(async () => {
        if (statusRef.current === 'loading') {
            return false;
        }
        if (statusRef.current === 'noMore') {
            return false;
        }
        setListStatus('loading');
        statusRef.current = 'loading';
        let res = await reachBottom();
        if (res && res.length) {
            newPageCreat.current = true;
            trueData.current.push(res);
            virtualTrueRef.current.push(res);
            setVirtualTrueData([...virtualTrueRef.current]);
            setTimeout(() => {
                const listEl: any = document.getElementsByClassName('visible-list');
                const height = listEl[listEl.length - 1].offsetHeight || 2000;
                listEl[listEl.length - 1].style.height = `${height}px`;
                // @ts-ignore
                contentObserver.current.observe(listEl[listEl.length - 1]);
            }, 0);
        } else {
            setListStatus('noMore');
            statusRef.current = 'noMore';
        }
    }, []);


    useEffect(() => {
        if (virtualTrueData.length >= maxPages) {
            setListStatus('noMore');
            statusRef.current = 'noMore';
        } else {
            setListStatus('more');
            statusRef.current = 'more';
        }
    }, [virtualTrueData, maxPages]);

    return (
        <div className='wuxiangundong'>
            <div className="visible-list-box">
                {virtualTrueData?.map((item, index) => (
                    <div key={index} id={String(index)} className="visible-list">
                        {item &&
                            item?.map((childItem, childIndex) => (
                                <div key={childItem[key] || childIndex}>
                                    {itemRender(childItem, index, childIndex)}
                                </div>
                            ))}
                    </div>
                ))}
                <div className="list-footer" style={{ minHeight: '100px' }}>
                    <LoadMore status={listStatus} />
                </div>
            </div>
        </div>
    );
}
