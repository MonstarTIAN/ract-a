/*
 * @Author: Monstar 10187267+monstartian@user.noreply.gitee.com
 * @Date: 2022-09-17 09:36:25
 * @LastEditors: Monstar 10187267+monstartian@user.noreply.gitee.com
 * @LastEditTime: 2022-09-17 09:44:54
 * @FilePath: \田瑞普\src\customHooks\loadMore.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// import styles from "./style-loadmore.module.css"
const components = {
    more: <div >上拉加载</div>,
    loading: (
        <div >
            <img
                src=""
                style={{ width: '44px', height: 'auto' }}
                alt="加载中"
            />
        </div>
    ),
    noMore: <div >已经到底了~</div>,
};

const LoadMore = ({ status = 'more' }: { status: 'more' | 'loading' | 'noMore' }) => {
    return <div>{components[status]}</div>;
};


export default LoadMore
