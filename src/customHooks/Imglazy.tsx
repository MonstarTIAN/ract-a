/*
 * @Author: Monstar 10187267+monstartian@user.noreply.gitee.com
 * @Date: 2022-09-17 09:05:48
 * @LastEditors: Monstar 10187267+monstartian@user.noreply.gitee.com
 * @LastEditTime: 2022-09-17 09:34:42
 * @FilePath: \田瑞普\src\customHooks\Imglazy.tsx
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import React, { useState, useEffect, useRef } from 'react';
const useImg = (target: any) => {
    // 声明一个变量控制是否开启
    let [isInViewport, setIsInViewport] = useState(false);
    // h5新增的api  IntersectionObserver
    const section = new IntersectionObserver(entries => {
        // 当节点触发 的时候去修改定义好的开关
        setIsInViewport(entries[0].isIntersecting);
    });
    // 监听target这个值的变化 当这个值变化的时候 去重新监听新的值
    useEffect(() => {
        target && section.observe(target);
    }, [target]);
    // 反馈是否开启
    return isInViewport;
}
const Img = (props: any) => {
    // 图片路径
    let [imgUrl, setImgUrl] = useState('');
    // 需要传递过去的dom元素
    let [target, setTarget] = useState(null);
    // 获取dom的更新
    const dom: any = useRef();
    // 拿到返回值 是否需要开启新的监听
    let isIn = useImg(target);

    useEffect(() => {
        // 修改target 为最新的dom
        setTarget(dom.current);
        // isIn是真的时候修改props.url重新赋值到img的src上实现图片的懒加载
        isIn && setImgUrl(props.url);
    }, [isIn]);
    return (
        <img ref={dom} src={imgUrl} />
    )
}