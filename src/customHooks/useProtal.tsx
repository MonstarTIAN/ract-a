import React, { useState, useCallback } from "react";
import Wrap from "./Wrap";
import type { ReactNode } from "react";
import ReactDOM from "react-dom";

interface ProtalOptions {
  closeOnOutSide?: boolean;
  className?: string;
  rootContainer?: HTMLElement;
  defaultShow?: boolean;
}

const useProtal = ({
  closeOnOutSide = true,
  className = "",
  rootContainer = document.body,
  defaultShow = false,
}: ProtalOptions = {}) => {
  const [isShow, setShow] = useState(defaultShow);

  // 关闭弹框
  const handleClose = useCallback(() => {
    setShow(false);
  }, []);
  // 打开弹框
  const handleShow = useCallback(() => {
    setShow(true);
  }, []);

  const Protal = useCallback(
    ({ children }: { children: ReactNode }) => {
      if (!isShow) return null;
      return ReactDOM.createPortal(
        <Wrap
          onClick={closeOnOutSide ? handleClose : () => { }}
          className={className}
        >
          {children}
        </Wrap>,
        rootContainer
      );
    },
    [isShow, className, rootContainer, closeOnOutSide]
  );
  return {
    Protal,
    isShow,
    handleClose,
    handleShow,
  };
};

export default useProtal;
